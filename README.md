###HiPagesTest

####System Requirements
1. Xcode 8.1 Swift 3
2. Cocoapods 1.0+
3. Run 
```
pod install
``` 
first

####Notice

1. The API key in the specs doesn't work, so I have to register another one which is defined in
```
ForecastService.swift
```

####Highlights
1. Automatic table row height
2. Universal app with both orientations
3. Typical MVC architecture

####Features not implemented
1. CLLocationManager to dynamically get the location rather than hard code Sydney's geo location
2. Didn't spend too much time in designing the UX, but just display the required information
3. Unit tests only cover the happy path
4. The timezone from the service response is Sydney/Australia, so other timezones haven't been covered.