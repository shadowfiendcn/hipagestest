//
//  WeatherHourlyData.swift
//  HiPagesTest
//
//  Created by Mei Ma on 23/11/16.
//  Copyright © 2016 Mei Ma. All rights reserved.
//

import Foundation
import ObjectMapper

class WeatherHourlyData : Mappable {
    
    // only list the properties that are required by the UI
    
    var summary : String?
    var data : [WeatherData]?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        summary <- map["summary"]
        data <- map["data"]
    }
    
}
