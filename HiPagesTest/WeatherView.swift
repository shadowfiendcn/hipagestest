//
//  WeatherView.swift
//  HiPagesTest
//
//  Created by Mei Ma on 23/11/16.
//  Copyright © 2016 Mei Ma. All rights reserved.
//

import Foundation
import UIKit

extension UIView {
    
    func addSubview(view:UIView, withEdgeInsets edgeInsets: UIEdgeInsets) {
        view.translatesAutoresizingMaskIntoConstraints = false
        self.addSubview(view)
        self.addConstraints([
            NSLayoutConstraint.init(item: self, attribute: .top, relatedBy: .equal, toItem: view, attribute: .top, multiplier: 1.0, constant: edgeInsets.top),
            NSLayoutConstraint.init(item: self, attribute: .bottom, relatedBy: .equal, toItem: view, attribute: .bottom, multiplier: 1.0, constant: edgeInsets.bottom),
            NSLayoutConstraint.init(item: self, attribute: .leading, relatedBy: .equal, toItem: view, attribute: .leading, multiplier: 1.0, constant: edgeInsets.left),
            NSLayoutConstraint.init(item: self, attribute: .trailing, relatedBy: .equal, toItem: view, attribute: .trailing, multiplier: 1.0, constant: edgeInsets.right)])
    }
    
}

class WeatherView: UIView {
    
    @IBOutlet var summaryLabel: UILabel!
    @IBOutlet var temperatureLabel: UILabel!
    @IBOutlet var humidityLabel: UILabel!
    @IBOutlet var windspeedLabel: UILabel!
    @IBOutlet var timeLabel: UILabel!
    
    let timeFormat = DateFormatter()

    static func viewFromXib() -> WeatherView {
        return Bundle.main.loadNibNamed("WeatherView", owner: nil, options: nil)?.first as! WeatherView
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        self.timeFormat.timeStyle = .short
    }
    
    func populate(weather: WeatherData, isCurrent: Bool)  {
        
        let notAvailableString = NSLocalizedString("N/A", comment: "")
        
        if isCurrent {
            
            self.timeLabel.text = NSLocalizedString("Now", comment: "")
            
        }
        else {
            if let time = weather.time {
                self.timeLabel.text = self.timeFormat.string(from: time)
            }
            else {
                self.timeLabel.text = notAvailableString
            }
            
        }
        
        self.summaryLabel.text = weather.summary ?? ""
        
        
        if let temperature = weather.temperature {
            self.temperatureLabel.text = String.init(format: "%.1f°F", temperature)
        }
        else {
            self.temperatureLabel.text = notAvailableString
        }
        
        if let humidity = weather.humidity {
            self.humidityLabel.text = String.init(format: "%.1f%%", humidity * 100)
        }
        else{
            self.humidityLabel.text = notAvailableString
        }
        
        if let windspeed = weather.windSpeed {
            self.windspeedLabel.text = String.init(format: "%.1f mi/h", windspeed)
        }
        else {
            self.windspeedLabel.text = notAvailableString
        }
        
    }

}
