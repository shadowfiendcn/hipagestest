//
//  WeatherCell.swift
//  HiPagesTest
//
//  Created by Mei Ma on 23/11/16.
//  Copyright © 2016 Mei Ma. All rights reserved.
//

import Foundation
import UIKit

class WeatherCell : UITableViewCell {
    
    private let weatherView = WeatherView.viewFromXib()
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.contentView.addSubview(view: self.weatherView, withEdgeInsets: .zero)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func populate(weather: WeatherData){
        self.weatherView.populate(weather: weather, isCurrent: false)
    }
}
