//
//  ForecastService.swift
//  HiPagesTest
//
//  Created by Mei Ma on 23/11/16.
//  Copyright © 2016 Mei Ma. All rights reserved.
//

import Foundation
import AlamofireObjectMapper
import Alamofire

class ForecastService {
    
    struct Constants {
        
        // please change the API_KEY if it doesn't work
        static let API_KEY = "2dab0908210b0a366956f26a75f37022"
        static let DOMAIN = "https://api.darksky.net"
    }
    
    func getForecastData(lat: Double, lon: Double, completionHandler: @escaping (ForecastData?, Error?) -> Void) {
        
        let serviceUrl = Constants.DOMAIN + "/forecast/"+Constants.API_KEY+"/"+String(lat)+","+String(lon)
        
        Alamofire.request(serviceUrl).responseObject { (response: DataResponse<ForecastData>) in
            switch response.result {
            case .success(let value):
                    completionHandler(value, nil)
            case .failure(let error):
                    completionHandler(nil, error)
            }
        }
    }
    
}
