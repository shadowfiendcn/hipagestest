//
//  ForecastData.swift
//  HiPagesTest
//
//  Created by Mei Ma on 23/11/16.
//  Copyright © 2016 Mei Ma. All rights reserved.
//

import Foundation
import ObjectMapper

class ForecastData : Mappable {
    
    // only list the properties that are required by the UI
    
    var currently : WeatherData?
    var hourly : WeatherHourlyData?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        currently <- map["currently"]
        hourly <- map["hourly"]
    }
}
