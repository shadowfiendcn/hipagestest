//
//  Weather.swift
//  HiPagesTest
//
//  Created by Mei Ma on 23/11/16.
//  Copyright © 2016 Mei Ma. All rights reserved.
//

import Foundation
import ObjectMapper

class WeatherData: Mappable {
    
    var time : Date?
    var temperature : Double?
    var humidity : Double?
    var windSpeed: Double?
    var summary : String?
    
    // only list the properties that are required by the UI
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        time <- (map["time"], DateTransform())
        temperature <- map["temperature"]
        humidity <- map["humidity"]
        windSpeed <- map["windSpeed"]
        summary <- map["summary"]
    }
    
}
