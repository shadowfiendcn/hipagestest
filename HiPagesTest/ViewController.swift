//
//  ViewController.swift
//  HiPagesTest
//
//  Created by Mei Ma on 23/11/16.
//  Copyright © 2016 Mei Ma. All rights reserved.
//

import UIKit



class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet var topView : UIView!
    @IBOutlet var refreshBarButtonItem: UIBarButtonItem!
    @IBOutlet var loadingSpinner: UIActivityIndicatorView!
    @IBOutlet var tableView: UITableView!
    
    let weatherView = WeatherView.viewFromXib()
    let forecastService = ForecastService()
    var hourlyWeatherData: [WeatherData]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.topView.addSubview(view: self.weatherView, withEdgeInsets: .zero)
        self.tableView.register(WeatherCell.classForCoder(), forCellReuseIdentifier: "WeatherCell")
        
        self.topView.alpha = 0
        self.tableView.alpha = 0
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: UITableViewDataSource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.hourlyWeatherData?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let weatherCell = tableView.dequeueReusableCell(withIdentifier: "WeatherCell", for: indexPath) as! WeatherCell
        
        if let weather = self.hourlyWeatherData?[indexPath.row]{
            weatherCell.populate(weather: weather)
        }
        
        return weatherCell
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if self.hourlyWeatherData == nil {
            loadData()
        }
    }
    
    
    // MARK: UITableViewDelegate
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return NSLocalizedString("Hourly", comment: "")
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 140
    }
    
    // MARK: IBAction
    @IBAction func didClickRefreshButton(_ sender: Any) {
        self.loadData()
    }
    
    // MARK: Helper
    func loadData() {
        
        // we can use the CLLocationManager to get the current lat and lon 
        
        // Sydney lat, lon
        let lat = -33.865143
        let lon = 151.209900
        
        self.startLoading()
        self.forecastService.getForecastData(lat: lat, lon: lon, completionHandler: {[weak self](forecastData, error) in
            self?.stopLoading()
            if let error = error {
                self?.showError(error: error)
            }
            else if let forecastData = forecastData {
                self?.populate(forecastData: forecastData)
            }
        })
    }
    
    func populate(forecastData: ForecastData)  {
        if let current = forecastData.currently {
            self.weatherView.populate(weather: current, isCurrent: true)
        }
        self.hourlyWeatherData = forecastData.hourly?.data
        self.tableView.reloadData()
        UIView.animate(withDuration: 0.3, animations: { [weak self] in
            self?.topView.alpha = 1.0
            self?.tableView.alpha = 1.0
            
        })

    }
    
    func showError(error: Error) {
        let alertController = UIAlertController.init(title: "Failed to get the weather forecast data", message: error.localizedDescription, preferredStyle: .alert)
        let okAction = UIAlertAction.init(title: NSLocalizedString("OK", comment: ""), style: .default, handler: nil)
        alertController.addAction(okAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func startLoading()  {
        let loadingBarButtonItem = UIBarButtonItem.init(customView: self.loadingSpinner)
        self.navigationItem.rightBarButtonItem = loadingBarButtonItem
    }
    
    func stopLoading()  {
        self.navigationItem.rightBarButtonItem = self.refreshBarButtonItem
    }
}

