//
//  HiPagesTestTests.swift
//  HiPagesTestTests
//
//  Created by Mei Ma on 23/11/16.
//  Copyright © 2016 Mei Ma. All rights reserved.
//

import XCTest

@testable import HiPagesTest

class HiPagesTestTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    // only test the good case
    func testGettingForecastData() {
        let forecastService = ForecastService()
        
        let exp = expectation(description: "test getting forecast data")
        
        forecastService.getForecastData(lat: 37.8267, lon: -122.4233, completionHandler: { (forecastData, error) in
            
            XCTAssertNil(error)
            XCTAssertNotNil(forecastData)
            
            print(forecastData?.toJSONString() ?? "No forecast data")
            
            exp.fulfill()
        })
        
        waitForExpectations(timeout: 10, handler: {error in
            
            if let error = error {
                let description = "The request failed with error: " + error.localizedDescription
                print(description)
                XCTFail()
            }
            
        })
        
        
    }
    
}
